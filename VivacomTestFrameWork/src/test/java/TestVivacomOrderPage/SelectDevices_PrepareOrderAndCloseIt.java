package TestVivacomOrderPage;

import com.testvivacom.testframework.Utils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SelectDevices_PrepareOrderAndCloseIt extends Base {

    @Test
    public void test_01_SelectADevice() {
        actions.waitForElementVisible("devicesMenu", 10);
        actions.clickElement("devicesMenu");
        actions.waitForElementVisible("phonesMenu", 10);
        actions.clickElement("phonesMenu");
        actions.assertElementPresent("manufacturerFilter");
        actions.clickElement("manufacturerHuawei");
        actions.assertElementPresent("deviceColorFilter");
        actions.clickElement("deviceColorFilterExpand");
        actions.waitForElementVisible("colorCrushBlue", 4);
        actions.clickElement("colorCrushBlue");
        actions.waitForElementVisible("deviceHuaweiNova5T", 10);
        actions.clickElement("deviceHuaweiNova5T");
        actions.waitForElementVisible("planFilter", 4);
        actions.scrollByVisibleElement("planSmartL");
        actions.clickElement("fullPriceOffer");
        actions.waitForElementVisible("addToCartButton", 10);
        actions.clickElement("addToCartButton");
        actions.waitForElementVisible("shoppingCartPage", 30);
        actions.assertElementPresent("shoppingCartPage");
        actions.scrollByVisibleElement("summarizeOrderField");
        actions.assertElementPresent("summarizeOrderField");
    }

    @Test
    public void test_02_SelectAnAccessory() {
        actions.clickElement("continuingShopping");
        actions.clickOnHiddenElement("vivacomLogo");
        actions.waitForElementClickable("devicesMenu", 10);
        actions.clickOnHiddenElement("devicesMenu");
        actions.waitForElementVisible("accessoriesMenu", 10);
        actions.clickElement("accessoriesMenu");
        actions.scrollByVisibleElement("accessoriesPriceFilter");
        actions.waitForElementVisible("accessoriesPriceMoreThan40", 4);
        actions.clickElement("accessoriesPriceMoreThan40");
        actions.scrollByVisibleElement("showMoreLink");
        actions.clickElement("showMoreLink");
        actions.scrollByVisibleElement("accessoryHeadphones");
        actions.clickOnHiddenElement("accessoryHeadphones");
        actions.waitForElementVisible("addToCartButton", 10);
        actions.clickElement("addToCartButton");
        actions.waitForElementClickable("summarizeOrderField", 15);
        actions.scrollByVisibleElement("summarizeOrderField");
        actions.assertElementPresent("summarizeOrderField");
    }

      @Test
    public void test_03_CorrectOrderWhenSumIsMoreThan600() {
      actions.waitForElementVisible("finalPrice", 30);
        double orderSum=Double.parseDouble(Utils.getUIMappingByKey("fullOrderSum"));
        if (orderSum>600){
            actions.clickElement("removeLastProductFromCartIcon");
            actions.assertElementNotPresent("accessoriesPrice");
        }
        else{
            actions.scrollByVisibleElement("creditEstimationCheckbox");
        } }

        @Test
        public void test_04_VerifyCaseContinueOrderBtnIsNotActive() {
            actions.waitForElementClickable("creditEstimationCheckbox", 5);
            actions.clickElement("creditEstimationCheckbox");
            try {
                actions.clickElement("continuingOrder");

            } catch (Exception e) {
                System.out.println("Continuing order link is not active.");
            }
        }
    @Test
    public void test_05_VerifyCaseContinueOrderBtnIsActive() {
        actions.waitForElementVisible("GDPRIcon",2);
        actions.clickElement("GDPRIcon");
        actions.waitForElementClickable("continuingOrder",2);
    }
    @Test
    public void test_06_VerifyNonLoggedMessage() {
            actions.clickElement("continuingOrder");
            actions.assertElementPresent("nonLoggedMessage");
        }
    @Test
    public void test_07_VerifyEmptyCartMessageAfterRemoveAllFromCart() {
        actions.waitForElementClickable("closePopupButton", 2);
        actions.clickElement("closePopupButton");
        actions.waitForElementVisible("summarizeOrderField", 5);
        actions.clickElement("removeProductFromCartIcon");
        actions.waitForElementVisible("emptyCartMessage", 5);
        actions.assertElementPresent("emptyCartMessage");

    }
}