package TestVivacomOrderPage;

import com.testvivacom.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class Base {
    UserActions actions = new UserActions();
    @BeforeClass
    public static void setUp(){
        UserActions.loadBrowser();
    }

    @AfterClass
    public static void tearDown(){
        UserActions.quitDriver();
    }
}
