package com.testvivacom.testframework;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.List;


public class UserActions {
	final WebDriver driver;

	public UserActions() {this.driver = Utils.getWebDriver();}

	public static void loadBrowser() {Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));}

	public static void quitDriver(){
		Utils.tearDownWebDriver();
	}

	public void clickElement(String key){
		Utils.LOG.info("Clicking on element " + key);
		waitForElementVisible(key, 10);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
		element.click();
	}
	public void clickOnHiddenElement(String key){
		Utils.LOG.info("Clicking on hiding element " + key);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		WebElement Element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
		js.executeScript("arguments[0].click();", Element);
	}
	public void scrollByVisibleElement(String key) {
		Utils.LOG.info("Scrolling until find the element " + key);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement Element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
		js.executeScript("arguments[0].scrollIntoView();", Element);
	}

	//############# WAITS #########
	public void waitForElementVisible(String locator, int seconds){
		WebDriverWait wait= new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
	}
	public void waitForElementClickable(String locator, int seconds) {
		WebDriverWait wait= new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(locator))));
	}
	//############# ASSERTS #########
	public void assertElementPresent(String locator){
		Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
	}
	public void assertElementNotPresent(String locator) {
		Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
	}
}