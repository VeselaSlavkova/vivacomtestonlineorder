package com.testvivacom.testframework;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class CustomWebDriverManager {
	public enum CustomWebDriverManagerEnum {
		INSTANCE;
		public WebDriver driver = setupBrowser();

		private WebDriver setupBrowser(){
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\ivo\\Desktop\\VivacomTestFrameWork\\src\\main\\resources\\chromedriver.exe");
			//or
			//ChromeDriverManager.getInstance().setup();
			WebDriver chromeDriver = new ChromeDriver();
			chromeDriver.manage().window().maximize();
			chromeDriver.manage().deleteAllCookies();
			return chromeDriver;
		}

		public void quitDriver() {
			if (driver != null) {
				driver.quit();
			}
		}

		public WebDriver getDriver() {
			return driver;
		}


	}
}
