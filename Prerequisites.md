## Set up Test Environment

1. OS       Windows 10 64-bit
2. Browser  v.Chrome79.0.3945.130
3. Java jdk1.8.0_221
4. Apache Maven Project 3.6.3
5. IntelliJ IDEA Community Edition 2019.2.3
6. ChromeDriver 79.0.3945.36 /need to type path to chromedriver.exe in *VivacomTestFrameWork\src\main\java\com\testvivacom\testframework\CustomWebDriverManager.java* /

## Run Test Suite *SelectDevices_PrepareOrderAndCloseIt*

1. Navigate to *VivacomTestFrameWork\Vivacom_TestFramework_Runner* and run *VivacomTests_Runner.bat*
2. Navigate to
*VivacomTestFrameWork\ target\ site* 
and open the *surefire-report.html* for results
